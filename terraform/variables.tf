variable "instance_type" {
  description = "EC2 instance type to use for the benchmarks."
  default     = "r7g.2xlarge"
}

variable "aws_profile" {
  description = "The AWS profile used to get the credentials from."
  default     = "default"
}

variable "key_name" {
  description = "`.pem` file name in your `.ssh` directory to SSH your EC2 instance."
  default = "benchmark-key-pair"
}

variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "eu-central-1"
}

variable "aws_buckets" {
  description = "The AWS S3 buckets to create and to use."
  default = ["sett-benchmarks", "sett-transfers"]
}

variable "volume_size" {
  description = "The size of the EBS volume in GB to perform the benchmarks on."
  default = 500
}
