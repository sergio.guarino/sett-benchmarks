# Terraform Scripts for sett benchmarks

**AWS** provisioning done by _main.tf_ has the following expectations:

1. Your `default` profile contains valid keys to access **AWS** environment.
2. You have a `.pem` SSH key registered on **AWS** to access the **EC2** instances.

Kindly adapt _variables.tf_ to your needs and run the following commands after you have installed **Terraform**:

```bash
terraform init
terraform validate
terraform plan
terraform apply
```

Once the **AWS** environment is set up, move to the **ansible** folder to run the benchmarks.

Remember to destroy the resources after you are done:

```bash
terraform destroy
```
