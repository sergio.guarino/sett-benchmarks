#!/usr/bin/env python

"""This program generates benchmarks images based on `hyperfine` input files. """

import argparse
import json
import os
from enum import Enum

import matplotlib.pyplot as plt


class BenchmarkEnum(Enum):
    LOCAL_ENCRYPT = 'local-encrypt'
    S3_ENCRYPT = 's3-encrypt'
    SFTP_ENCRYPT = 'sftp-encrypt'
    S3_TRANSFER = 's3-transfer'
    SFTP_TRANSFER = 'sftp-transfer'
    DECRYPT = 'decrypt'

    def __str__(self):
        return self.value

    @staticmethod
    def from_string(s):
        try:
            return BenchmarkEnum[s]
        except KeyError:
            raise ValueError()


sizes = [20, 300, 1000]

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("version", help="Parent folder (typically sett version number) "
                                    + "where hyperfine input files are located.")
args = parser.parse_args()


def create_plot(measures_by_env, environments, benchmark_type):
    plt.xlabel("File size [MB]")
    plt.ylabel("Time [s]")
    plt.title("Benchmark results for '{}'".format(benchmark_type))
    # Draw the plots
    for env in environments:
        plt.plot(sizes, measures_by_env[env], label=env, marker='o')
    # Draw the table
    means = [round(
        sum([size / measure for measure, size in zip(measures_by_env[env], sizes)]) / len(sizes), 2)
             for
             env in environments]
    table = plt.table(cellText=[means],
                      rowLabels=["Throughput [MB/s]"],
                      colWidths=[0.1] * 3,
                      colLabels=[folder for folder in environments],
                      loc='lower right')
    table.auto_set_font_size(False)
    table.set_fontsize(10)
    table.scale(1.5, 1.5)

    plt.legend()
    plt.savefig('{}.svg'.format(benchmark_type))
    plt.savefig('{}.png'.format(benchmark_type), dpi=300)
    plt.show()


def __main__():
    for benchmark_item in BenchmarkEnum:
        environments = ['local', 'aws', 'biomedit'] if not benchmark_item.value.startswith('sftp-') else ['local']
        measures_by_env = {env: [] for env in environments}
        for env in environments:
            for size in sizes:
                f = os.path.join(args.version, os.path.join(env, '{}-{}mb.json'.format(benchmark_item.value, size)))
                with open(f) as open_file:
                    data = json.load(open_file)
                    measures_by_env[env].append(data['results'][0]['mean'])
        create_plot(measures_by_env, environments, benchmark_item)


if __name__ == '__main__':
    __main__()
