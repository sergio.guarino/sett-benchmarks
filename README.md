# sett benchmarks
- [Terraform](./terraform/README.md) for setting up the **AWS** environment.
- [Ansible](./ansible/README.md) for running the benchmarks.
- [Results](./results/README.md) for the benchmark results and plots
